//librerie
#include <ESP8266WiFi.h>
#include <PubSubClient.h>
#include <SR04.h>
#include <Servo.h>

//VAR x ULTRASUONI
#define TRIG_PIN 12 //D6 nella esp
#define ECHO_PIN 14 //D5 nella esp
SR04 sr04 = SR04(ECHO_PIN,TRIG_PIN);

//VAR SERVO
Servo myservo;  
int posServo = 0; 
boolean opened = false; 

//VAR RGB LED
#define BLUE 5 //D1
#define GREEN 4 //D2
#define RED 0 //D3

//VAR x programma
int pushButton = D7; //per reset
int buttonTrain = D4; //per training
int stato = 0; //cambi di stato
int tAvanti = 0; 
int tIndietro = 0; 
String codice = "";
String codici[10]; //lista di codici
int posPuntatore=0; //pos in codici

int distance1=0;
int distance2=0;
double Speed=0;
int distance=0;
long tStart = 0; //tempo di partenza
long tCurrent = 0; //tempo corrente

const int minDist = 7; //distanza per avanti
const int maxDist = 100; //dist per indietro

//interi di stato
const int STATO_INIZIALE = 0;
const int STATO_AVANTI = 1;
const int STATO_APRI = 2;
const int STATO_ADDMOSSA = 3;
const int STATO_CHIUDI = 4;
const int STATO_RESET = 5;
const int STATO_TRAIN = 6;

//VAR X MQTT 
const char* ssid = "***";
const char* password = "***";
const char* mqtt_server = "broker.hivemq.com";
char codiceUtente[11];
boolean copia = false;
char c; //lettera singola
WiFiClient espClient;
PubSubClient client(espClient);

//FUNZIONI
void setup_wifi() {
  delay(10);
  Serial.println();
  Serial.print("Connessione a");
  Serial.println(ssid);
  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  randomSeed(micros());
  Serial.println("");
  Serial.println("WiFi connected");
}

void svuotaCod(){
  for( int u = 0; u < sizeof(codiceUtente);  ++u ){
    codiceUtente[u] = (char)0;
  } 
}

void callback(char* topic, byte* payload, unsigned int length) {
  if(stato != STATO_ADDMOSSA && stato != STATO_AVANTI){
    if(length > 7){  
      Serial.println("Messaggio troppo lungo");
      client.publish("ProvaAccessMoveRisposte", "Messaggio troppo lungo");
     }else{
        for (int t = 0; t < length; t++) {
          c = (char)payload[t];
          if(c == 'A'||c == 'I'||c == 'V'||c == 'P'||c == 'L'||c == 'S'){
            //se t = 0 controlla anche che inizi con un avanti
            if(t==0 && c!= 'A'){
             Serial.println("Prima mossa Avanti, ritenta"); 
             client.publish("ProvaAccessMoveRisposte", "Prima mossa Avanti, ritenta");
             svuotaCod();
              break;
            }else{
              codiceUtente[t]=(char)payload[t];
            }
          }else{
            Serial.println("Codice non valido, ritenta");
            client.publish("ProvaAccessMoveRisposte", "Codice non valido, ritenta");
            //ripulisci codiceUtente
            svuotaCod();
            break;
          }
        }
        codice = String(codiceUtente);
        if(codice != ""){
          stato = STATO_TRAIN;
          svuotaCod();
        }
    }
  }else{
    client.publish("ProvaAccessMoveRisposte", "Ritenta più tardi"); 
   }
}

void reconnect() {
  while (!client.connected()) {
    Serial.print("Attempting MQTT connection...");
    String clientId = "ESP8266Client-";
    clientId += String(random(0xffff), HEX);
    if (client.connect(clientId.c_str())) {
      Serial.println("Connected");
      client.publish("ProvaAccessMoveRisposte", "Connected");
      client.subscribe("ProvaAccessMove");
    } else {
      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");
      delay(5000);
    }
  }
}

void stampaLista(){
    for(int e=0; e <10; e++){
    Serial.print("__ ");
    Serial.print(e);
    Serial.print("__ ");
    Serial.print(codici[e]);
  }
}

void ICACHE_RAM_ATTR ripristina(){
  //Se cliccato, azzerami il codice
  //se sei nel mezzo del servo, lascia così
  if(stato != STATO_APRI && stato != STATO_CHIUDI){
      stato=STATO_RESET;
  }
}
void ICACHE_RAM_ATTR training(){
  if(stato != STATO_APRI && stato != STATO_CHIUDI){
    stato=STATO_TRAIN;
  }
}


void openDoor(){
  for (posServo = 0; posServo <= 180; posServo += 1) { 
    myservo.write(posServo);              
    delay(15);               
  }
  stato = STATO_INIZIALE;
  opened = true;
}

void closeDoor(){
  for (posServo = 180; posServo >= 0; posServo -= 1){ 
    myservo.write(posServo);  
    delay(15);                       
  }
  stato = STATO_INIZIALE;
  opened = false;
}

void lightOn(int red, int green, int blue){
  analogWrite(RED,red );
  analogWrite(GREEN, green);
  analogWrite(BLUE, blue);
}
void lightOff(){
  analogWrite(RED,0 );
  analogWrite(GREEN, 0);
  analogWrite(BLUE, 0);
}

void colora(char mossa){
  switch(mossa){
    case 'A':
      lightOn(0, 255, 0);
      delay(1000); //time light on
      lightOff();
    break;
    case 'I':
      lightOn(255, 0, 0);
      delay(1000);
      lightOff();
    break;
    case 'V':
      lightOn(255, 255, 0);
      delay(1000);
      lightOff();
    break;
    case 'P':
      lightOn(0, 0, 255);
      delay(1000);
      lightOff();;
    break;
    case 'L':
      lightOn(255, 0, 255);
      delay(1000);
      lightOff();
    break;
    case 'S':
      lightOn(255, 51, 0);
      delay(1000);
      lightOff(); 
    break;
    case 'Q':
      lightOn(255, 255, 255);
      delay(250);
      lightOff();
    break;
  }
}

void azzera(){
   tIndietro = 0;
   tAvanti = 0;
   distance1 = 0;
   distance2 = 0;
   tStart = 0;
   tCurrent = 0;
}

void gesture(){
  //Aggiunge la lettera corrispondente al codice e controlla se c'è in lista
  if(tStart == 0){ //se il tempo di partenza è zero = prima misurazione valida
      tStart = millis(); //prendi il primo tempo
      colora('Q');
      Serial.println(tStart);
      distance1 = distance; //prendi la prima distanza valida
    }else{
      tCurrent = millis(); //prendi il tempo corrente
      if(tCurrent-tStart >= 1500){
       distance2 = distance;
       Speed = (distance2 - distance1)/1.5;
       /*Serial.print("Velocità: "); 
       Serial.print(Speed);
       Serial.println("cm/s");
       */
       if(Speed < 0){ //verso US
        if(Speed < -10){
          //più veloce
          codice.concat("V");
          colora('V');
          Serial.println(codice);
        }else{
         //più lento
         codice.concat("P");
         colora('P');
         Serial.println(codice);    
        }
       }else{ 
        //lontano
        if(Speed >10){
          //più veloce
          codice.concat("S");
          colora('S');
          Serial.println(codice);
        }else{
         //più lento
         codice.concat("L");
         colora('L');
         Serial.println(codice);
        }
       }
       if(esiste()){
          stato = STATO_APRI;
          codice = "";
          Serial.println("codice riconosciuto");
          client.publish("ProvaAccessMoveRisposte", "Codice riconosciuto");    
       }
       azzera();
       delay(1000);
      }  
    }
  }

boolean esiste(){
 for(int i = 0; i< 10; i++){
  if(codice == codici[i]){
    return true;
  }
 }
 return false;
}

void setup() {
  Serial.begin(115200);
  pinMode(RED, OUTPUT);
  pinMode(GREEN, OUTPUT);
  pinMode(BLUE, OUTPUT);
  setup_wifi();
  client.setServer(mqtt_server, 1883);
  client.setCallback(callback);
  myservo.attach(15); 
  pinMode(buttonTrain, INPUT_PULLUP);
  pinMode(pushButton, INPUT_PULLUP);
  attachInterrupt(
    digitalPinToInterrupt(pushButton),
    ripristina,   
    RISING   
   );
   attachInterrupt(
    digitalPinToInterrupt(buttonTrain),
    training,   
    RISING  
   );
}

void loop() {
  switch(stato){
    case STATO_INIZIALE:
     distance = sr04.Distance();
     if(distance < minDist && stato != STATO_APRI){
      tAvanti++;
     }
     if(tAvanti == 20){ //tempo sufficiente
       tAvanti = 0; 
       stato = STATO_AVANTI;
     }
    break;
    case STATO_AVANTI:
     codice.concat("A");
     colora('A');
     Serial.println(codice);
     if(esiste()){
      stato = STATO_APRI;
      Serial.print("codice riconosciuto_");
      client.publish("ProvaAccessMoveRisposte", "Codice riconosciuto");
      Serial.println(codice);
      codice = "";
     }else{
      Serial.println(stato);
      if(opened){
        stato = STATO_CHIUDI;
        codice="";
      }else{
        stato = STATO_ADDMOSSA;
      }
    }
    break;  
    case STATO_APRI:
      openDoor();
    break;
    case STATO_ADDMOSSA:
     distance=sr04.Distance();
     if(codice.length()> 7){
       Serial.println("Codice non riconosciuto");
       codice="";
       stato = STATO_INIZIALE;
     }
     if(distance < minDist && stato != STATO_APRI){
      tAvanti++;
      Serial.print("°");
     }
     if(tAvanti == 20){
       Serial.println("°");
       azzera();
       stato = STATO_AVANTI;
     }
     if(distance < maxDist && distance > minDist){ //se distanza è ragionevole
      gesture();
     }
     if(distance > maxDist){
      tIndietro++;
      Serial.print("*");
     }
     if(tIndietro == 50){ 
      //tempo necessario, fino a quando tIndietro non arriva a 50
      codice.concat("I");
      colora('I');
      Serial.println("*");
      Serial.println(codice);
      azzera();
      delay(2000); //attesa
      if(esiste()){
          stato = STATO_APRI;
          codice = "";
          Serial.println("codice riconosciuto");
          client.publish("ProvaAccessMoveRisposte", "Codice riconosciuto");
       }
     }
   break; 
   case STATO_CHIUDI:
     closeDoor();
   break;
   case STATO_RESET:
    Serial.println(codice);
    codice="";
    stato = STATO_INIZIALE;
    tAvanti = 0;
    tIndietro = 0;
    Serial.println("RESET CODICE stato 5");
    client.publish("ProvaAccessMoveRisposte", "Reset codice");
   break; 
   case STATO_TRAIN:
    Serial.println(codice);
    if(codice==""){
      Serial.println("Codice vuoto");
      client.publish("ProvaAccessMoveRisposte", "Codice vuoto");
    }else{
       if(posPuntatore <10){
         if(esiste()){
            Serial.print("Codice già inserito, ritenta");
            client.publish("ProvaAccessMoveRisposte", "Codice già inserito, ritenta");
          }else{
            codici[posPuntatore]= codice;
            codice.toCharArray(codiceUtente, 11);
            client.publish("ProvaAccessMoveRisposte","codice inserito");
            client.publish("ProvaAccessMoveRisposte",codiceUtente);
            posPuntatore++;
            stampaLista();
            svuotaCod();
         }
      }else{
          Serial.print("array pieno");
          client.publish("ProvaAccessMoveRisposte", "Lista Piena");
      }
    }
    codice="";
    stato = STATO_INIZIALE;
    tAvanti = 0;
    tIndietro = 0;
    Serial.println("Preso CODICE stato 6");
   break;
  }

 if (!client.connected()) {
    reconnect();
 }
 client.loop();
 
}



  
