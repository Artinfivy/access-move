# Access Move

Il progetto consiste in una porta (simulata da un'asticella attaccata ad un servo) che si apre se l'utente esegue 
un corretto codice di movimenti (ad esempio: avanti,indietro,avanti) di fronte al sensore a ultrasuoni.

### Hardware

- ESP8266
- Servo motore SG90
- Sensore a ultrasuoni  HC-SR04
- Led RGB
- Power supply
- Pulsanti

Il prototipo prevede che l'utente possa eseguire 6 mosse:
- Avanti vicino
- Indietro 
- Avvicinamento lento 
- Avvicinamento veloce
- Allontanamento lento
- Allontanamento veloce

Il led comunicano all'utente quali passi sono stati rilevati dal sensore. Poi, una volta rilevati i passi sufficenti a completare 
uno dei codici memorizzati nella board, il motore si avvia e si apre la porta.
Quando viene rivelato il passaggio di una persona (avanti vicino), si richiude.
Per poter essere riconosciuto, ogni codice inserito deve incominciare con un passo avanti.
I codici vengono inseriti tramite MQTT in un array. Ho aggiunto un pulsante che permette di creare il codice direttamente con il sensore e inserirlo nella lista come gli altri.
Inoltre, ho voluto aggiungere un pulsante che resettasse il codice qualora l'utente volesse crearlo da capo.

***Circuito***
![](Images/CircuitoRGB.png)


### Software

Per creare il programma ho preso ispirazione dal ragionamento per creare una macchina a stati finiti.
Tramite uno Switch-case, il programma si sposta in diversi stati, rappresentati dalla variabile stato.

- Stato 0: Stato iniziale, l'utente può solo far iniziare il codice. 
Per incominciare, l'utente deve fare un passo avanti, il quale viene riconosciuto come prima mossa e si 
passa allo stato 1.
- Stato 1: Inizia a memorizzare il codice, se riconosce un codice, passa allo stato 2. Altrimenti si 
passa allo stato 3 per continuare ad aggiungere mosse. Infine, 
si passa allo stato 4 se la porta è aperta e deve essere richiusa.
Ogni volta che viene riconosciuta una mossa, il led si colora di un colore specifico.
Ad esempio, se si identifica un passo avanti, il led si illumina di verde. 

+ `#00ff00` A: avanti vicino 

- Stato 2: Si apre la porta, ovvero aziona il servo, poi passa allo stato 0 una volta finito.
- Stato 3: Qui si crea il codice vero e proprio, ogni lettera corrisponde a una mossa.

+ `#ff0000` I: indietro
+ `#0000ff` P: avanzo piano
+ `#ffff00` V: avanzo veloce
+ `#ff00ff` L: indietreggio piano
+ `#ff3300` S: indietreggio veloce	

Se viene riconosciuto un passo in Avanti, si ritorna allo stato 1.
Per ogni altro movimento, identificato da una lettera, che viene riconosciuto, 
si testa se il codice formato sino a quel punto corrisponde a qualche codice nella lista, 
se così fosse, si passa allo stato 2, altrimenti si continua fino a 8 mosse, 
e se fino alla fine non verrà riconosciuto, allora si ritorna 
allo stato 0.

- Stato 4: Chiudo la porta,poi passo allo stato 0.
- Stato 5: azionato il primo pulsante con interrupt, resetto il codice, passo allo stato 0
- Stato 6: azionato il secondo pulsante, memorizzo il codice nell'array se non presente e si ritorna allo stato 0.

**Legenda**

Diagramma<br>
Una FSM ha una rigorosa esplicazione matematica, mi sono limitata ad adottare alcune regole
dei diagrammi di stato delle FSM per dare una rappresentazione del funzionamento del software.<br>
In questo caso, la FSM è composta da una tupla (S, I, O, U, P) dove:<br>
S: insieme finito di stati: { Iniziale,Avanti, Apri, AddMossa, Chiudi, Reset, Training}<br>
I: Insieme finito di eventi di input: {<br>
reset = interrupt pulsante reset,<br>
training = Interrupt pulsante di training<br>
distanza = distanza rilevata dal sensore<br>
distMin = distanza < 7cm, viene riconosciuto un ostacolo davanti al sensore per tempo sufficiente<br>
opened/closed = porta aperta o chiusa,<br>
codCorretto = codice riconosciuto<br>
}

O: insieme degli output: {<br>
codVuoto = codice vuoto<br>
codAdd = codice a cui è stata aggiunta una lettera<br>
codice = codice completo<br>
} <br>

P: funzione di input/putput:<br>
{  (Iniziale, reset, codVuoto, S5),
 (Avanti, reset, codice, Reset),
 (AddMossa, reset,codice, Reset),
 (Iniziale, training, codVuoto, Training),
 (Avanti, training, codice, Training),
 (AddMossa, training, codice, Training),
 (Reset, Iniziale),
 (Apri, opened,codVuoto, Iniziale), 
 (Chiudi, closed, codVuoto, Iniziale), 
 (Training, Iniziale), 
 (Iniziale, distMin, codVuoto, Avanti), 
 (Avanti, codCorretto, Apri), 
 (Avanti, opened, codice, Chiudi), 
 (Avanti, codAdd, AddMossa), 
 (AddMossa, distMin, Avanti), 
 (AddMossa, codCorretto, Apri), 
 (AddMossa, distanza,codAdd, AddMossa)
 (AddMossa,codVuoto, Iniziale)
}
***Diagramma***

![](Images/DiagrammaStati.png)

<img src="Images/Proj.jpg" width="300"/>

<img src="Images/ProjDettaglio.jpg" alt="dettaglio" width="300"/>
